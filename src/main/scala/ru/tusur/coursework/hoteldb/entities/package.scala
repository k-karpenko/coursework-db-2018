package ru.tusur.coursework.hoteldb

import java.time.Instant

package object entities {

  sealed trait DomainEntity

  case class Id[T <: DomainEntity](id: String)

  case class Room(id: String, number: Int, level: Int) extends DomainEntity

  case class Booking(id: String, createdAt: Instant, closedAt: Option[Instant]) extends DomainEntity

  case class Service(id: String, name: String, roomId: Option[String]) extends DomainEntity

  case class ServicePrice(serviceId: String, price: Double, startsAt: Instant, activeUntil: Option[Instant]) extends DomainEntity

  case class ServiceOrder(orderId: String, serviceId: String, orderedAt: Instant, orderPrice: Double) extends DomainEntity

  case class Guest(id: String, fullName: String, passportSeries: String, passportNumber: String) extends DomainEntity

  case class Personnel(id: String, fullName: String) extends DomainEntity

}
