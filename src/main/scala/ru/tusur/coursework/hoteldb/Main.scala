package ru.tusur.coursework.hoteldb

import ru.tusur.coursework.hoteldb.console.MainProgram

import scala.util.{Failure, Success, Try}

object Main {

  def readSection(min: Int, max: Int): Option[Int] = {
    def loop(attempt: Int): Option[Int] = {
      println("Введние номер секции для продолжения:")
      val sectionNumber = Console.in.readLine()
      Try(sectionNumber.toInt).filter(_ <= max).filter(_ >= min) match {
        case Success(value) => Some(value)
        case Failure(_) if attempt < 4 =>
          Console.err.println("[ОШИБКА] Номер секции введен некорректно")
          loop(attempt + 1)
        case Failure(_) =>
          None
      }
    }

    loop(0)
  }

  def main(args: Array[String]): Unit = {
    MainProgram.run
  }

}
