package ru.tusur.coursework.hoteldb

import java.time.LocalDate
import java.util.Date

import ru.tusur.coursework.hoteldb.Main.readSection
import ru.tusur.coursework.hoteldb.console.MainProgram.handleInput

import scala.util.Try

package object console {

  trait ConsoleProgram {
    val label: String

    def run(): Unit

    protected def printHeader(value: String): Unit = {
      println(value)
      println("===================================================")
    }

    protected def printMenuItems(items: Seq[String]): Unit = {
      items.zipWithIndex foreach { case (item, idx) =>
        println(s"${idx + 1}. $item")
      }
    }

    protected def printError(message: String): Unit = {
      Console.err.println(s"[ОШИБКА] $message")
    }

    protected def readAndValidate[T](message: String, validate: String => Either[String, Unit], transform: String => T): Option[T] = {
      def loop(attempts: Int): Option[T] = {
        println(message)
        val value = Console.in.readLine()
        validate(value) match {
          case Right(_) => Some(transform(value))
          case Left(errorMessage) if attempts < 4 =>
            printError(errorMessage)
            loop(attempts + 1)
          case Left(errorMessage) =>
            printError(errorMessage)
            None
        }
      }

      loop(0)
    }

    protected def handleInput(map: Seq[(ConsoleProgram, Int)]): Unit = {
      for {
        sectionNumber <- readSection(1, 5)
        program = map.find(_._2 == sectionNumber - 1)
      } yield program match {
        case Some((p, _)) =>
          p.run()
        case None =>
          System.exit(0)
      }
    }
  }

  object MainProgram extends ConsoleProgram {
    override val label: String = "Консоль Управления"

    override def run(): Unit = {
      printHeader("КОНСОЛЬ УПРАВЛЕНИЯ ОТЕЛЕМ v.0.1-alpha")

      printMenuItems(mainPrograms map (_.label))

      handleInput(mainPrograms.zipWithIndex)
    }
  }

  object Reservations extends ConsoleProgram {
    override val label: String = "Управление резервированием"

    override def run(): Unit = {
      printHeader("МОДУЛЬ УПРАВЛЕНИЯ РЕЗЕРВИРОВАНИЯМИ")

      printMenuItems(reservationPrograms map (_.label))
      handleInput(reservationPrograms.zipWithIndex)
    }
  }

  object Rooms extends ConsoleProgram {
    override val label: String = "Управление номерами"

    def run() = println("Номер отеля")
  }

  object Services extends ConsoleProgram {
    override val label: String = "Управление услугами"

    def run() = println("Сервисы")
  }

  object Guests extends ConsoleProgram {
    override val label: String = "Доступ к списку постояльцев"

    def run() = println("Гости")
  }

  object Personnel extends ConsoleProgram {
    override val label: String = "Управление персоналом отеля"

    def run() = println("Персонал")
  }

  object MakeReservation extends ConsoleProgram {
    override val label: String = "Создание новое резервирование"

    override def run(): Unit = {
      printHeader("РЕЗЕРВИРОВАНИЕ НОМЕРА")

      val result = for {
        checkInDate <- readAndValidate[LocalDate]("Дата въезда:", value =>
          Try(LocalDate.parse(value)).toOption match {
            case Some(_) => Right({})
            case None => Left("Дата введена некорректно")
          }, LocalDate.parse(_))
        checkOutDate <- readAndValidate[LocalDate]("Дата выезда:", value => {
          val result = Try(LocalDate.parse(value)).toOption
          result match {
            case None => Left("Дата введена некорректно")
            case Some(v) =>
              if (!v.isAfter(checkInDate)) Left("Дату выезда необходимо указывать в будущем относительно даты въезда")
              else Right({})
          }
        }, LocalDate.parse(_))
      } yield println("Резервируем номер...")

      result match {
        case None => println("Ошибка резервирования")
        case Some(_) => println("Резервирование успешно")
      }
    }
  }

  object ListReservations extends ConsoleProgram {
    override val label: String = "Список доступных резервирований"

    override def run(): Unit = ???
  }

  object UpdateReservation extends ConsoleProgram {
    override val label: String = "Внести изменение в резервирование"

    override def run(): Unit = ???
  }

  object CancelReservation extends ConsoleProgram {
    override val label: String = "Отмена ранее созданного резервирования"

    override def run(): Unit = ???
  }

  val mainPrograms: List[ConsoleProgram] = List(Reservations, Rooms, Services, Guests, Personnel)
  val reservationPrograms: List[ConsoleProgram] = List(MakeReservation, ListReservations, UpdateReservation, CancelReservation)
}
