package ru.tusur.coursework.hoteldb

import java.time.Instant

import ru.tusur.coursework.hoteldb.entities._

import scala.concurrent.Future

package object services {

  def listBookings: Future[Seq[Booking]] = ???

  def listAvailableRooms(date: Instant): Future[Seq[Room]] = ???

  def listGuests: Future[Seq[Guest]] = ???

  def listRoomServices(roomId: Id[Room]): Future[Seq[Service]] = ???

  def makeBooking(roomNumber: Id[Room], dateStart: Instant, dateEnd: Instant, additionalServices: Seq[Id[Service]]): Future[Id[Booking]] = ???

}
